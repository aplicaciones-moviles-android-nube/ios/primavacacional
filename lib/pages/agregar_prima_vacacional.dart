import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_01_ejercicio/items/item_prima_vacacional.dart';
import 'package:intl/intl.dart';

/*
WIDGET PARA AGREGAR ITEMS EN LA LISTA DE PRIMA VACACIONAL
 */

class AgregarPrimaVacacional extends StatefulWidget {
  final PrimaVacacionalItem? item;
  const AgregarPrimaVacacional({Key? key, required this.onAgregar, this.item})
      : super(key: key);
  final Function(PrimaVacacionalItem) onAgregar;

  @override
  State<AgregarPrimaVacacional> createState() => _AgregarPrimaVacacionalState();
}

class _AgregarPrimaVacacionalState extends State<AgregarPrimaVacacional> {
//CONTROLADORES
  final _nombreController = TextEditingController();
  final _apellidoController = TextEditingController();
  final _sueldoMensualController = TextEditingController();
  final _diasVacacionesController = TextEditingController();
  final _porcentajeController = TextEditingController();
  double _primaVacacionalBruta = 0;

  //funcion para calular
  /*void _calcular() {
    setState(() {
      _primaVacacionalBruta =
          ((double.tryParse(_sueldoMensualController.text)! / 30) *
                  int.tryParse(_diasVacacionesController.text)!) *
              (double.tryParse(_porcentajeController.text)! / 100);
    });
  }*/
  void _calcular() {
    final sueldoMensual = double.tryParse(_sueldoMensualController.text);
    final diasVacaciones = int.tryParse(_diasVacacionesController.text);
    final porcentaje = double.tryParse(_porcentajeController.text);

    if (sueldoMensual != null && diasVacaciones != null && porcentaje != null) {
      setState(() {
        _primaVacacionalBruta =
            (sueldoMensual / 30) * diasVacaciones * (porcentaje / 100);
      });
    }
  }

  //Agregar init state

  @override
void initState() {
  super.initState();
  if (widget.item != null) {
    final nombreCompleto = widget.item!.nombreCompleto;
    final partesNombre = nombreCompleto.split(' ');

    if (partesNombre.isNotEmpty) {
      _nombreController.text = partesNombre.first;
      if (partesNombre.length == 2) {
        _apellidoController.text = partesNombre.skip(1).join(' ');
      } else if (partesNombre.length >= 2) {
        _nombreController.text = partesNombre.take(2).join(' ');
        _apellidoController.text = partesNombre.skip(2).join(' ');
      } else {
        _apellidoController.text = '';
      }
    } else {
      _nombreController.text = '';
      _apellidoController.text = '';
    }
    _sueldoMensualController.text =
        widget.item!.sueldoMensualBruto.toString();
    _diasVacacionesController.text = widget.item!.diasVacaciones.toString();
    _porcentajeController.text =
        widget.item!.porcentajePrimaVacacional.toString();
    _primaVacacionalBruta = widget.item!.primaVacacionalBruta;
  }

  _nombreController.addListener(_calcular);
  _apellidoController.addListener(_calcular);
  _sueldoMensualController.addListener(_calcular);
  _diasVacacionesController.addListener(_calcular);
}


  @override
  void dispose() {
    _nombreController.removeListener(_calcular);
    _apellidoController.removeListener(_calcular);
    _sueldoMensualController.removeListener(_calcular);
    _diasVacacionesController.removeListener(_calcular);
    // se debe llamar hasta el final
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: const Text("Agregar Prima Vacacional"),
        trailing: CupertinoButton(
            padding: EdgeInsets.zero,
            child: Icon(
              widget.item == null
                  ? CupertinoIcons.add_circled
                  : CupertinoIcons.pencil,
            ),
            onPressed: () {
              if (widget.item == null) {
                widget.onAgregar(PrimaVacacionalItem(
                    nombreCompleto:
                        '${_nombreController.text} ${_apellidoController.text}',
                    sueldoMensualBruto:
                        double.tryParse(_sueldoMensualController.text)!,
                    diasVacaciones:
                        int.tryParse(_diasVacacionesController.text)!,
                    porcentajePrimaVacacional:
                        int.tryParse(_porcentajeController.text)!,
                    primaVacacionalBruta: _primaVacacionalBruta));
              } else {
                widget.onAgregar(PrimaVacacionalItem(
                    nombreCompleto:
                        '${_nombreController.text} ${_apellidoController.text}',
                    sueldoMensualBruto:
                        double.tryParse(_sueldoMensualController.text)!,
                    diasVacaciones:
                        int.tryParse(_diasVacacionesController.text)!,
                    porcentajePrimaVacacional:
                        int.tryParse(_porcentajeController.text)!,
                    primaVacacionalBruta: _primaVacacionalBruta));
              }
            }),
      ),
      child: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Nombre + Apellido
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Nombre:"),
                        CupertinoTextField(
                          controller: _nombreController,
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("Apellido:"),
                        CupertinoTextField(
                          controller: _apellidoController,
                        )
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 8,
              ),
              const Text("Sueldo Mensual Bruto:"),
              CupertinoTextField(
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d*'))
                ],
                controller: _sueldoMensualController,
              ),
              const SizedBox(
                height: 8,
              ),
              const Text("Dias de vacaciones:"),
              CupertinoTextField(
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                controller: _diasVacacionesController,
              ),
              const SizedBox(
                height: 8,
              ),
              const Text("Porcentaje de Prima vacacional:"),
              CupertinoTextField(
                controller: _porcentajeController,
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              ),
              const SizedBox(
                height: 30,
              ),
              const Divider(
                height: 1,
                indent: 20,
                endIndent: 20,
              ),
              const SizedBox(
                height: 30,
              ),
              const Text(
                "Prima Vacacional",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Empleado:"),
                  Text("${_nombreController.text} ${_apellidoController.text}")
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Prima Vacacional:"),
                  Text(NumberFormat.currency(symbol: "\$")
                      .format(_primaVacacionalBruta))
                ],
              ),
            ],
          ),
        ),
      )),
    );
  }
}
