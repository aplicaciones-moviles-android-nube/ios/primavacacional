import 'package:flutter/cupertino.dart';
import 'dart:math';
///import 'package:flutter/material.dart';


///
/// Los widgets statefuls tienen dos clases
///
///1. La clase que extiende de StatefulWidget->Se va encarga de definir el estado
///2. La clase que extiende de Sate -> Se encarga de construir el widgt y manejar el estado
///
///StatefulWidget -> Inmutable
///State -> Mutable (Puedo cambiar sus propiedades internas)
///

class CambioColorWidget extends StatefulWidget {
  const CambioColorWidget({super.key});

  @override
  State<CambioColorWidget> createState() => _CambioColorWidgetState();
}

class _CambioColorWidgetState extends State<CambioColorWidget> {
  Color _color = CupertinoColors.destructiveRed;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CupertinoButton(
          child: const Text("Cambiar color"),
          onPressed: () {
            setState(() {
              _color = Color.fromRGBO(
                Random().nextInt(255),
                Random().nextInt(255),
                Random().nextInt(255),
                1,
              );
            });
          },
        ),
        Container(width: 100, height: 100, color: _color),
      ],
    );
  }
}

class ContadorWidget extends StatefulWidget {
  final int valorInicial; // este no cambia
  const ContadorWidget({super.key, required this.valorInicial});
  @override
  State<ContadorWidget> createState() => _ContadorWidget();
}

class _ContadorWidget extends State<ContadorWidget> {
  int _contador = 0; //este si cambia

  @override
  void initState() {
    super.initState();
    _contador = widget.valorInicial;
  }

  void add() {
    setState(() {
      _contador++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CupertinoButton(
            child: Text("$_contador : Añadir"),
            onPressed: () => {
                  setState(() {
                    _contador++;
                  })
                })
      ],
    );
  }
}
