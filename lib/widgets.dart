/*
Dos tipos de Widgets

SateslesWidget:
Todo su contendido es inmutable(no cambian), son utilez en componentes de tipo de
textos,iconos, botones, algo que se renderiza y no tnemos que estemos cambiando cada vez
si tuviera estado flutter tendria que calcular cada rato, esto se traduce en optimizacion

Stateful:
Pueden cambiar su estado interno
Slider,Switch,Textfield
*/

///import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_01_ejercicio/statefull/stateful.dart';
import 'package:flutter_01_ejercicio/stateless/stateless.dart';

/*

 */
class MiAplicacion extends StatelessWidget {
  //es identificador unico que flutter utiliza para volver a pintar los elementos, seria como
  //la reconstruccion del widget en el arbol de Widgets de flutter
  const MiAplicacion({super.key});

  @override
  Widget build(BuildContext context) {
    return const CupertinoApp(
      title: "Mi Aplicacion",
      home: MiHomePage(),
      debugShowCheckedModeBanner: false,
    //  theme: CupertinoThemeData(brightness: Brightness.dark),

    );
  }
}

class MiHomePage extends StatelessWidget {
  const MiHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Mi pagina principal"),
      ),
      child: SafeArea(
        child: Column(
          
          children: <Widget>[
            Text("Texto 1 "),
            Text("Texto 2 "),
            Text("Texto 3 "),
            Texto(texto: "Mi texto en Widget"),
            Lista(),
            NewsCard(title: "Titulo", source: "Mileno", date: "Domingo", imagePath: "assets/images/news1.jpg"),
            CambioColorWidget(),
            ContadorWidget(valorInicial: 20)
          ],
        ),
      ),
    );
  }
}
