import 'package:flutter/cupertino.dart';
import 'package:flutter_01_ejercicio/pages/home_prima_vacacional.dart';

class PrimaVacacionalApp extends StatelessWidget {
  const PrimaVacacionalApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CupertinoApp(
      home: HomePrimaVacacional(),
      title: 'Prima vacacional',
      theme: CupertinoThemeData(
        primaryColor: CupertinoColors.systemIndigo,
        brightness: Brightness.light,
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}




